
import com.sun.jna.Library;
import com.sun.jna.Native;


public class Main {
    public interface CDouble extends Library {
        CDouble INSTANCE = (CDouble) Native.load("double",CDouble.class);

        int douple(int value);
    }

    public static void main(String[] args) {
        int n = 55;
        System.out.println(System.getProperty("sun.arch.data.model") );
        System.out.println(String.format("Double (or douple as it is named)of %d is %d.", n, CDouble.INSTANCE.douple(n)));

    }
}