#![crate_type= "cdylib"]

#[no_mangle]
fn douple(n: i32) -> i32 {
    n * 2i32
}